#!/bin/sh
set -e
set +x
echo "Building helm chart."

echo "Installing yq..."
wget https://github.com/mikefarah/yq/releases/download/v4.23.1/yq_linux_amd64 -O /usr/bin/yq && chmod +x /usr/bin/yq

if [[ ! -z $CI_COMMIT_BRANCH ]]; then
  version=0.0.0-$CI_COMMIT_BRANCH
  echo Setting chart version for branch build...
  version=$version yq e -i '.version = env(version)' orientdb/Chart.yaml
fi

if [[ ! -z $CI_COMMIT_TAG ]]; then
  if [[ $(expr match "$CI_COMMIT_TAG" 'version\/.*') != 0 ]]; then
    version=$(echo $CI_COMMIT_TAG | sed "s/^version\///g")
    if [[ $version != $(yq e '.version' orientdb/Chart.yaml) ]]; then
      echo Release version does not match chart version!
      exit 1
    fi
  else
    echo Skipping to build tag $CI_COMMIT_TAG.
    exit 0
  fi
fi

if [[ ! -z $version ]]; then
  echo "Building chart (version: $version)..."
  helm registry login -u $CI_REGISTRY_USER -p $CI_REGISTRY_PASSWORD $CI_REGISTRY
  chart_name=$(helm package ./orientdb | sed 's:.*/::')
  helm push $chart_name oci://$CI_REGISTRY_IMAGE
fi
